package csu.coderwu.tool.es.bean;


import csu.coderwu.tool.es.constant.Week;

/**
 * 课程
 * @author coderWu
 * Created in 下午9:42 17-12-10
 */
public class Course extends EsBase {

    private String teacherName;
    private String classRoom;
    private String beginTime;
    private String endTime;
    private String courseName;
    private String week;

    public Course(){}

    public Course(Student student){
        this.student = student;
    }


    public String getTeacherName() {
        return teacherName;
    }

    public Course setTeacherName(String teacherName) {
        this.teacherName = teacherName;
        return this;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public Course setClassRoom(String classRoom) {
        this.classRoom = classRoom;
        return this;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public Course setBeginTime(String beginTime) {
        this.beginTime = beginTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public Course setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getCourseName() {
        return courseName;
    }

    public Course setCourseName(String courseName) {
        this.courseName = courseName;
        return this;
    }

    public String getWeek() {
        return week;
    }

    public Course setWeek(String week) {
        this.week = week;
        return this;
    }

    public Course setWeek(char week) {
        switch (week) {
            case '1' : {
                this.week = Week.MONDAY;
                return this;
            }
            case '2' : {
                this.week = Week.TUESDAY;
                return this;
            }
            case '3' : {
                this.week = Week.WEDNESDAY;
                return this;
            }
            case '4' : {
                this.week = Week.THURSDAY;
                return this;
            }
            case '5' : {
                this.week = Week.FRIDAY;
                return this;
            }
            case '6' : {
                this.week = Week.SATURDAY;
                return this;
            }
            case '7' : {
                this.week = Week.SUNDAY;
                return this;
            }
            default:
                return this;
        }

    }
}
