package csu.coderwu.tool.es.api;

import csu.coderwu.tool.es.bean.*;

import java.util.List;

/**
 * @author coderWu
 * Created in 下午3:39 17-12-8
 */
public interface EsService {

    /**
     * 登录检测
     * @param xh 学号
     * @param pwd 密码
     * @return boolean
     */
    boolean loginCheck(String xh, String pwd);

    /**
     * 获取app token
     * @param xh 学号
     * @param pwd 密码
     * @return token
     */
    String getToken(String xh, String pwd);

    /**
     * 获取学生信息
     * @param xh 学号
     * @param pwd 密码
     * @return Student || null
     */
    Student getStudentInfo(String xh, String pwd);

    /**
     * 获取课程表
     * @param xh 学号
     * @param pwd 密码
     * @param semester 学期
     * @param week 第几周
     * @return CourseSchedule || null
     */
    CourseSchedule getCourseSchedule(String xh, String pwd, String semester, int week);

    /**
     * 自习室查询
     * @param campusId 校区编号
     * @return List<StudyRoom> || null
     */
    List<StudyRoom> getStudyRoom(String campusId);

    /**
     * 成绩查询
     * @param xh 学号
     * @param pwd 密码
     * @param semester 学期
     * @return List<Score> || null
     */
    List<Score> getScore(String xh, String pwd, String semester);

    /**
     * 检测学号姓名是否匹配
     * @param xh 学号
     * @param name 姓名
     * @return boolean
     */
    boolean checkXhAndName(String xh, String name);

    boolean accessible();

    /**
     * 获取教学日期{学期，周次等}
     * @return SchoolDate
     */
    SchoolDate getSchoolData();
}
