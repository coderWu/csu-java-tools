package csu.coderwu.tool.es.constant;

/**
 * @author coderWu
 * Created in 下午2:17 17-12-9
 */
public class EsResponse {

    public static final String INVALID_TOKEN = "-1";

    public static final int INVALID_RESPONSE_LENGTH = 16;

}
