package csu.coderwu.tool.es.exception;

/**
 * @author : coderWu
 * @date : Created on 14:33 2018/5/19
 */
public class EsException extends RuntimeException {

    public EsException(String message) {
        super(message);
    }
}
