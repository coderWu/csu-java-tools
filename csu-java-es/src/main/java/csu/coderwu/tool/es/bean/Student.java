package csu.coderwu.tool.es.bean;

import csu.coderwu.tool.common.bean.BaseStudent;

/**
 * @author coderWu
 * Created in 下午2:34 17-12-9
 */
public class Student extends BaseStudent {


    /**
     * 教务系统登陆密码
     */
    private String password;
    /**
     * 学制
     */
    private Integer xz;
    /**
     * 班级名称
     */
    private String className;
    /**
     * 年级
     */
    private String grade;
    /**
     * 专业名称
     */
    private String zy;

    private String gender;
    /**
     * 学院名称
     */
    private String yxmc;

    @Override
    public Student setXh(String xh) {
        super.setXh(xh);
        return this;
    }

    @Override
    public Student setName(String name) {
        super.setName(name);
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Student setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Student setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getYxmc() {
        return yxmc;
    }

    public Student setYxmc(String yxmc) {
        this.yxmc = yxmc;
        return this;
    }

    public Integer getXz() {
        return xz;
    }

    public Student setXz(Integer xz) {
        this.xz = xz;
        return this;
    }

    public String getClassName() {
        return className;
    }

    public Student setClassName(String className) {
        this.className = className;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public Student setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getZy() {
        return zy;
    }

    public Student setZy(String zy) {
        this.zy = zy;
        return this;
    }
}
