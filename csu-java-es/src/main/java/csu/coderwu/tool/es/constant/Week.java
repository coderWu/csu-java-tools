package csu.coderwu.tool.es.constant;

/**
 * @author coderWu
 * Created in 下午11:29 17-12-10
 */
public class Week {

    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String SUNDAY = "sunday";

}
