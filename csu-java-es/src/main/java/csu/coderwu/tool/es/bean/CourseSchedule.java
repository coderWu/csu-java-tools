package csu.coderwu.tool.es.bean;

import csu.coderwu.tool.es.constant.Week;
import java.util.ArrayList;
import java.util.List;

/**
 * 课程表
 * @author coderWu
 * Created in 下午9:51 17-12-10
 */
public class CourseSchedule extends EsBase {

    private String semester;
    private int week;
    private List<Course> monday = new ArrayList<>(3);
    private List<Course> tuesday = new ArrayList<>(3);
    private List<Course> wednesday = new ArrayList<>(3);
    private List<Course> thursday = new ArrayList<>(3);
    private List<Course> friday = new ArrayList<>(3);
    private List<Course> saturday = new ArrayList<>(3);
    private List<Course> sunday = new ArrayList<>(3);

    public String getSemester() {
        return semester;
    }

    public CourseSchedule setSemester(String semester) {
        this.semester = semester;
        return this;
    }

    public int getWeek() {
        return week;
    }

    public CourseSchedule setWeek(int week) {
        this.week = week;
        return this;
    }

    public CourseSchedule(){}

    public CourseSchedule(Student student) {
        this.student = student;
    }

    public List<Course> getMonday() {
        return monday;
    }

    public CourseSchedule setMonday(List<Course> monday) {
        this.monday = monday;
        return this;
    }

    public List<Course> getTuesday() {
        return tuesday;
    }

    public CourseSchedule setTuesday(List<Course> tuesday) {
        this.tuesday = tuesday;
        return this;
    }

    public List<Course> getWednesday() {
        return wednesday;
    }

    public CourseSchedule setWednesday(List<Course> wednesday) {
        this.wednesday = wednesday;
        return this;
    }

    public List<Course> getThursday() {
        return thursday;
    }

    public CourseSchedule setThursday(List<Course> thursday) {
        this.thursday = thursday;
        return this;
    }

    public List<Course> getFriday() {
        return friday;
    }

    public CourseSchedule setFriday(List<Course> friday) {
        this.friday = friday;
        return this;
    }

    public List<Course> getSaturday() {
        return saturday;
    }

    public CourseSchedule setSaturday(List<Course> saturday) {
        this.saturday = saturday;
        return this;
    }

    public List<Course> getSunday() {
        return sunday;
    }

    public CourseSchedule setSunday(List<Course> sunday) {
        this.sunday = sunday;
        return this;
    }

    public CourseSchedule addCourse(Course course) {
        switch (course.getWeek()) {
            case Week.MONDAY : {
                monday.add(course);
                return this;
            }
            case Week.TUESDAY : {
                tuesday.add(course);
                return this;
            }
            case Week.WEDNESDAY : {
                wednesday.add(course);
                return this;
            }
            case Week.THURSDAY : {
                thursday.add(course);
                return this;
            }
            case Week.FRIDAY : {
                friday.add(course);
                return this;
            }
            case Week.SATURDAY : {
                saturday.add(course);
                return this;
            }
            case Week.SUNDAY : {
                sunday.add(course);
                return this;
            }
            default:
                return this;
        }
    }
}
