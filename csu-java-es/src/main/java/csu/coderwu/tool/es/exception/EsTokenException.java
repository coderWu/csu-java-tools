package csu.coderwu.tool.es.exception;

/**
 * @author : coderWu
 * @date : Created on 14:31 2018/5/19
 */
public class EsTokenException extends EsException {

    public EsTokenException(String message) {
        super(message);
    }
}
