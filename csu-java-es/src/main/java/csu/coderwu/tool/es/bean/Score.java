package csu.coderwu.tool.es.bean;

/**
 * @author coderWu
 * Created in 下午9:54 17-12-10
 */
public class Score extends EsBase {

    private String courseName;
    /**
     * 部分成绩用优良表示
     */
    private String score;
    /**
     * 学分
     */
    private Double credit;
    private String scoreType;
    private String courseType;

    public Score(){}

    public Score(Student student) {
        this.student = student;
    }

    public String getCourseName() {
        return courseName;
    }

    public Score setCourseName(String courseName) {
        this.courseName = courseName;
        return this;
    }

    public String getScore() {
        return score;
    }

    public Score setScore(String score) {
        this.score = score;
        return this;
    }

    public Double getCredit() {
        return credit;
    }

    public Score setCredit(Double credit) {
        this.credit = credit;
        return this;
    }

    public String getScoreType() {
        return scoreType;
    }

    public Score setScoreType(String scoreType) {
        this.scoreType = scoreType;
        return this;
    }

    public String getCourseType() {
        return courseType;
    }

    public Score setCourseType(String courseType) {
        this.courseType = courseType;
        return this;
    }
}
