package csu.coderwu.tool.es.bean;

/**
 * 自习室
 * @author coderWu
 * Created in 下午10:19 17-12-10
 */
public class StudyRoom {

    private String buildingName;
    private String roomName;
    private String campusName;
    private int seat;

    public String getCampusName() {
        return campusName;
    }

    public StudyRoom setCampusName(String campusName) {
        this.campusName = campusName;
        return this;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public StudyRoom setBuildingName(String buildingName) {
        this.buildingName = buildingName;
        return this;
    }

    public String getRoomName() {
        return roomName;
    }

    public StudyRoom setRoomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public int getSeat() {
        return seat;
    }

    public StudyRoom setSeat(int seat) {
        this.seat = seat;
        return this;
    }
}
