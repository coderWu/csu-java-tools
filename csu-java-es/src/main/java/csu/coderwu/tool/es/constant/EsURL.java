package csu.coderwu.tool.es.constant;

/**
 * @author coderWu
 * Created in 下午3:38 17-12-8
 */
public class EsURL {

    public static final String IP = "202.197.61.221";

    private static final String ROOT = "http://csujwc.its.csu.edu.cn";

    public static final String LOGIN = ROOT + "/app.do?method=authUser&xh=%s&pwd=%s";

    public static final String STUDENT_INFO = ROOT + "/app.do?method=getUserInfo&xh=%s";

    public static final String COURSE_SCHEDULE = ROOT + "/app.do?method=getKbcxAzc&xh=%s&xnxqid=%s&zc=%d";

    public static final String SCORE = ROOT + "/app.do?method=getCjcx&xh=%s&xnxqid=%s";

    public static final String STUDY_ROOM = ROOT + "/app.do?method=getKxJscx&xqid=%s";

    public static final String SCHOOL_DATE = ROOT + "/app.do?method=getCurrentTime&currDate=%s";
}
