package csu.coderwu.tool.es.bean;

/**
 * 教务系统信息基础类，返回的信息中需要包含Student信息
 * @author : coderWu
 * @date : Created on 11:18 2018/5/19
 */
public class EsBase {

    protected Student student;

    public Student getStudent() {
        return student;
    }

    public EsBase setStudent(Student student) {
        this.student = student;
        return this;
    }
}
