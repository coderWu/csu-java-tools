package csu.coderwu.tool.es.constant;

/**
 * @author coderWu
 * Created in 下午2:15 17-12-9
 */
public class ResponseKey {

    public static final String TOKEN = "token";

    public static final String STUDENT_NAME = "xm";
    public static final String STUDENT_GENDER = "xb";
    public static final String STUDENT_YXMC = "yxmc";
    public static final String STUDENT_XH = "xh";
    public static final String STUDENT_XZ = "xz";
    public static final String STUDENT_CLASS = "bj";
    public static final String STUDENT_GRADE = "nj";
    public static final String STUDENT_ZY = "zymc";

    public static final String COURSE_BEGIN_TIME = "kssj";
    public static final String COURSE_END_TIME = "jssj";
    public static final String COURSE_CLASSROOM = "jsmc";
    public static final String COURSE_TEACHER = "jsxm";
    public static final String COURSE_NAME = "kcmc";
    public static final String COURSE_DAY_TIME = "kcsj";

    public static final String BUILDING_NAME = "jzwmc";
    public static final String CAMPUS_NAME = "xqmc";
    public static final String STUDY_ROOM_LIST = "jsList";
    public static final String STUDY_ROOM_NAME = "jsmc";
    public static final String STUDY_ROOM_SEAT = "zws";

    public static final String SCORE_COURSE_NAME = "kcmc";
    public static final String SCORE = "zcj";
    public static final String SCORE_CREDIT = "xf";
    public static final String SCORE_TYPE = "ksxzmc";
    public static final String SCORE_COURSE_TYPE = "kclbmc";

    public static final String SCHOOL_DATE_WEEK = "zc";
    public static final String SCHOOL_DATE_WEEK_BEGIN = "s_time";
    public static final String SCHOOL_DATE_WEEK_END = "e_time";
    public static final String SCHOOL_SEMESTER = "xnxqh";
}
