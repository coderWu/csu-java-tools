package csu.coderwu.tool.es.exception;

/**
 * @author : coderWu
 * @date : Created on 14:43 2018/5/19
 */
public class EsConnectException extends EsException {

    public EsConnectException(String message) {
        super(message);
    }
}
