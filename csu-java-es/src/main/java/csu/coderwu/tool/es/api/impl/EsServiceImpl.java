package csu.coderwu.tool.es.api.impl;

import csu.coderwu.tool.es.api.EsService;
import com.github.kevinsawicki.http.HttpRequest;
import csu.coderwu.tool.common.constant.HttpRequestConstant;
import csu.coderwu.tool.es.bean.*;
import csu.coderwu.tool.es.constant.EsRequestHeader;
import csu.coderwu.tool.es.constant.EsResponse;
import csu.coderwu.tool.es.constant.EsURL;
import csu.coderwu.tool.es.constant.ResponseKey;
import csu.coderwu.tool.es.exception.EsConnectException;
import csu.coderwu.tool.es.exception.EsException;
import csu.coderwu.tool.es.exception.EsTokenException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author coderWu
 * Created in 下午3:39 17-12-8
 */
public class EsServiceImpl implements EsService {

    private final Logger log = LoggerFactory.getLogger(EsServiceImpl.class);

    private String defaultXh;
    private String defaultPwd;

    public String getDefaultXh() {
        return defaultXh;
    }

    public EsServiceImpl setDefaultXh(String defaultXh) {
        this.defaultXh = defaultXh;
        return this;
    }

    public String getDefaultPwd() {
        return defaultPwd;
    }

    public EsServiceImpl setDefaultPwd(String defaultPwd) {
        this.defaultPwd = defaultPwd;
        return this;
    }

    public EsServiceImpl() {}

    public EsServiceImpl(String defaultXh, String defaultPwd) {
        this.defaultXh = defaultXh;
        this.defaultPwd = defaultPwd;
    }

    @Override
    public boolean loginCheck(String xh, String pwd) {
        return !getToken(xh, pwd).equals(EsResponse.INVALID_TOKEN);
    }

    @Override
    public String getToken(String xh, String pwd) {
        String url = String.format(EsURL.LOGIN, xh, pwd);
        String body =  sendRequest(null, null, url);
        JSONObject json = new JSONObject(body);
        String token;
        if (json.has(ResponseKey.TOKEN) &&
                !(token = (String) json.get(ResponseKey.TOKEN)).equals(EsResponse.INVALID_TOKEN)) {
            return token;
        }
        throw new EsTokenException("用户名或密码不正确");
    }

    @Override
    public Student getStudentInfo(String xh, String pwd) {
        String url = String.format(EsURL.STUDENT_INFO, xh);
        String body = sendRequest(xh, pwd, url);

        JSONObject json = new JSONObject(body);

        return new Student().setClassName(json.getString(ResponseKey.STUDENT_CLASS))
                .setGrade(json.getString(ResponseKey.STUDENT_GRADE))
                .setXh(json.getString(ResponseKey.STUDENT_XH))
                .setXz(json.getInt(ResponseKey.STUDENT_XZ))
                .setZy(json.getString(ResponseKey.STUDENT_ZY))
                .setGender(json.getString(ResponseKey.STUDENT_GENDER))
                .setName(json.getString(ResponseKey.STUDENT_NAME))
                .setYxmc(json.getString(ResponseKey.STUDENT_YXMC));
    }

    @Override
    public CourseSchedule getCourseSchedule(String xh, String pwd, String semester, int week) {
        String url = String.format(EsURL.COURSE_SCHEDULE, xh, semester, week);
        String body = sendRequest(xh, pwd, url);

        CourseSchedule courseSchedule = new CourseSchedule();
        JSONArray json = new JSONArray(body);
        for (int i = 0, l = json.length(); i < l; i++) {
            Course course = new Course();
            JSONObject item = (JSONObject) json.get(i);
            course.setWeek(item.getString(ResponseKey.COURSE_DAY_TIME).charAt(0))
                    .setBeginTime(item.getString(ResponseKey.COURSE_BEGIN_TIME))
                    .setEndTime(item.getString(ResponseKey.COURSE_END_TIME))
                    .setClassRoom(item.getString(ResponseKey.COURSE_CLASSROOM))
                    .setTeacherName(item.getString(ResponseKey.COURSE_TEACHER))
                    .setCourseName(item.getString(ResponseKey.COURSE_NAME));
            courseSchedule.addCourse(course);
        }
        courseSchedule.setSemester(semester);
        courseSchedule.setWeek(week);
        return courseSchedule;
    }

    @Override
    public List<StudyRoom> getStudyRoom(String campusId) {

        if (defaultPwd == null || defaultPwd.equals("")
                || defaultXh == null || defaultXh.equals("")) {
            this.log.error("默认学号或密码为空");
            throw new EsException("默认学号或密码为空");
        }

        String url = String.format(EsURL.STUDY_ROOM, campusId);
        String body = sendRequest(defaultXh, defaultPwd, url);

        JSONArray json = new JSONArray(body);
        int jsonLength = json.length();
        List<StudyRoom> allRooms = new ArrayList<>();
        for (int i = 0; i < jsonLength; i++) {
            JSONObject item = json.getJSONObject(i);
            JSONArray buildingRooms = item.getJSONArray(ResponseKey.STUDY_ROOM_LIST);
            for (int j = 0, l = buildingRooms.length(); j < l; j++) {
                StudyRoom room = new StudyRoom();
                JSONObject roomJson = buildingRooms.getJSONObject(j);
                room.setBuildingName(roomJson.getString(ResponseKey.BUILDING_NAME))
                        .setRoomName(roomJson.getString(ResponseKey.STUDY_ROOM_NAME))
                        .setSeat(roomJson.getInt(ResponseKey.STUDY_ROOM_SEAT))
                        .setCampusName(roomJson.getString(ResponseKey.CAMPUS_NAME));
                allRooms.add(room);
            }
        }

        return allRooms;
    }

    @Override
    public List<Score> getScore(String xh, String pwd, String semester) {
        String url = String.format(EsURL.SCORE, xh, semester);
        String body = sendRequest(xh, pwd, url);

        JSONArray allScoresJson = new JSONArray(body);
        int scoreCount = allScoresJson.length();
        List<Score> allScoreList = new ArrayList<>(scoreCount);
        for (int i = 0; i < scoreCount; i++) {
            JSONObject scoreJson = allScoresJson.getJSONObject(i);
            Score score = new Score()
                    .setScore(scoreJson.getString(ResponseKey.SCORE))
                    .setCourseName(scoreJson.getString(ResponseKey.SCORE_COURSE_NAME))
                    .setScoreType(scoreJson.getString(ResponseKey.SCORE_TYPE))
                    .setCourseType(scoreJson.getString(ResponseKey.SCORE_COURSE_TYPE))
                    .setCredit(scoreJson.getDouble(ResponseKey.SCORE_CREDIT));
            allScoreList.add(score);
        }
        return allScoreList;
    }

    @Override
    public boolean checkXhAndName(String xh, String name) {

        if (defaultPwd == null || defaultPwd.equals("")
                || defaultXh == null || defaultXh.equals("")) {
            this.log.error("默认学号密码为空");
            return false;
        }

        if (xh == null || name == null || "".equals(xh) || "".equals(name)) {
            this.log.warn("查询学号或密码为空");
            return false;
        }

        String url = String.format(EsURL.STUDENT_INFO, xh);
        String body = sendRequest(this.defaultXh, this.defaultPwd, url);

        JSONObject student = new JSONObject(body);
        String getName =
                student.has(ResponseKey.STUDENT_NAME) ? student.getString(ResponseKey.STUDENT_NAME) : "";
        return !"".equals(getName) && getName.equals(name);
    }

    @Override
    public boolean accessible() {
        try {
            return InetAddress.getByName(EsURL.IP).isReachable(HttpRequestConstant.CONNECT_TIME);
        } catch (Exception e) {
            this.log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public SchoolDate getSchoolData() {
        if (defaultPwd == null || defaultPwd.equals("")
                || defaultXh == null || defaultXh.equals("")) {
            this.log.error("默认学号密码为空");
            throw new EsException("默认学号或密码为空");
        }
        String nowDate = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        String url = String.format(EsURL.SCHOOL_DATE, nowDate);
        String body = sendRequest(this.defaultXh, this.defaultPwd, url);
        JSONObject schoolDateJson = new JSONObject(body);
        if (schoolDateJson.has(ResponseKey.SCHOOL_DATE_WEEK)) {
            return new SchoolDate()
                    .setSemester(schoolDateJson.getString(ResponseKey.SCHOOL_SEMESTER))
                    .setWeekNumber(schoolDateJson.getInt(ResponseKey.SCHOOL_DATE_WEEK));
        } else {
            return null;
        }
    }

    private String sendRequest(String xh, String pwd, String url) {
        String body;
        try {
            body = HttpRequest
                    .get(url)
                    .connectTimeout(HttpRequestConstant.CONNECT_TIME)
                    .header(EsRequestHeader.TOKEN,
                            (xh == null || pwd == null) ? "" : getToken(xh, pwd))
                    .body();
        } catch (Exception e) {
            this.log.error("教务系统连接失败： {} ---- {}", e.getMessage(), url);
            throw new EsConnectException(e.getMessage());
        }
        body = body == null ? "{}" : body;
        if (body.length() <= EsResponse.INVALID_RESPONSE_LENGTH) {
            this.log.info("未能获取信息，请检查默认学号密码是否正确");
            throw new EsException("未能获取信息，请检查默认学号密码是否正确");
        }
        return body;
    }

}
