package csu.coderwu.tool.es.bean;

/**
 * 学校日期
 * @author coderWu
 * Created in 下午9:59 17-12-10
 */
public class SchoolDate {

    private int weekNumber;
    private String semester;

    public int getWeekNumber() {
        return weekNumber;
    }

    public SchoolDate setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
        return this;
    }

    public String getSemester() {
        return semester;
    }

    public SchoolDate setSemester(String semester) {
        this.semester = semester;
        return this;
    }
}
