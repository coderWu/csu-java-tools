package csu.coderwu.tool.cet.api;

import csu.coderwu.tool.common.bean.BaseStudent;
import csu.coderwu.tool.cet.bean.CetScore;

import java.util.List;

/**
 * @author coderWu
 * @date 17-12-20 下午7:20
 */
public interface CetService {

    /**
     * 查询历史成绩
     * @param xh 学号
     * @param xm 姓名
     * @return null || List<CetScore>
     */
    List<CetScore> getHistoryScores(String xh, String xm);


    List<CetScore> getHistoryScores(BaseStudent student);

    boolean accessible();
}
