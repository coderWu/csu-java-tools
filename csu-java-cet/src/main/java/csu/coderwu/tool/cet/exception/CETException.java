package csu.coderwu.tool.cet.exception;

/**
 * @author : coderWu
 * @date : Created on 11:37 2018/5/19
 */
public class CETException extends RuntimeException {

    public CETException(String s) {
        super(s);
    }

}
