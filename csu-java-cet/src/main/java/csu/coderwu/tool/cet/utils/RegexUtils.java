package csu.coderwu.tool.cet.utils;

import csu.coderwu.tool.cet.bean.CetScore;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : coderWu
 * @date : Created on 11:30 2018/5/19
 */
public class RegexUtils {

    private static final Pattern CHECK_EXAM_NUMBER =
            Pattern.compile("((?<=/td>\r\n             <td>)[\\w\\W]*?(?=<))");
    private static final Pattern CHECK_CERTIFICATE_ID =
            Pattern.compile("((?<=[0-9]{6,}</td>\r\n             <td>)[\\w\\W]*?(?=   </td>\r\n\r\n             <td>))");
    private static final Pattern CHECK_SCORE =
            Pattern.compile("((?<=/td>\r\n\r\n             <td>)[\\w\\W]*?(?=<))");


    public static List<CetScore> matchCetGrade(String content) {

        if (content == null || "".equals(content)) {
            return null;
        }

        List<CetScore> scores = new ArrayList<>();
        String pattern = "((?<=engfendy)[\\w\\W]*?(?=ffaf60))";
        String[] splitContent = content.split(pattern, 0);

        String date;
        String examNumber;

        for (String subString : splitContent) {

            Matcher number = CHECK_EXAM_NUMBER.matcher(subString);
            Matcher cerId = CHECK_CERTIFICATE_ID.matcher(subString);
            Matcher score = CHECK_SCORE.matcher(subString);

            if (number.find() && cerId.find() && score.find()) {

                examNumber = number.group();
                date = "20" + examNumber.substring(6, 8) + "-";
                date += examNumber.charAt(8) == '1' ? "6" : "12";

                CetScore cetScore = new CetScore();
                cetScore.setExamNumber(examNumber)
                        .setCertificateId(cerId.group())
                        .setScore(Integer.valueOf(score.group()))
                        .setLevel(examNumber.charAt(9) == '1' ? "四级" : "六级")
                        .setDate(date);
                scores.add(cetScore);
            }
        }

        return scores;
    }

}
