package csu.coderwu.tool.cet.api.impl;

import csu.coderwu.tool.cet.api.CetService;
import csu.coderwu.tool.common.bean.BaseStudent;
import csu.coderwu.tool.cet.bean.CetScore;
import com.github.kevinsawicki.http.HttpRequest;
import csu.coderwu.tool.cet.constant.CETURL;
import csu.coderwu.tool.common.constant.HttpRequestConstant;
import csu.coderwu.tool.cet.exception.CETException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import csu.coderwu.tool.cet.utils.RegexUtils;

import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author coderWu
 * @date 17-12-20 下午7:20
 */
public class CetServiceImpl implements CetService {

    private final Logger log = LoggerFactory.getLogger(CetService.class);

    /**
     * 查询历史成绩
     * @param xh 学号
     * @param xm 姓名
     * @return null || List<CetScore>
     */
    @Override
    public List<CetScore> getHistoryScores(String xh, String xm) {
        return getHistoryScores(new BaseStudent(xh, xm));
    }

    @Override
    public List<CetScore> getHistoryScores(BaseStudent student) {
        String body;
        String xm = student.getName();
        String xh = student.getXh();
        try {
            xm = URLEncoder.encode(xm, "gb2312");
            body = HttpRequest.get(CETURL.CET_HISTORY, false, "xh", xh, "xm", xm)
                    .connectTimeout(HttpRequestConstant.CONNECT_TIME)
                    .body();
        } catch (Exception e) {
            this.log.error("四六级历史成绩-校园网链接失败");
            throw new CETException("四六级历史成绩-校园网链接失败");
        }
        List<CetScore> scores = RegexUtils.matchCetGrade(body);

        if (scores == null) {
            return null;
        }

        for (CetScore score : scores) {
            score.setStudent(student);
        }
        return scores;
    }

    @Override
    public boolean accessible() {
        try {
            return InetAddress.getByName(CETURL.IP).isReachable(HttpRequestConstant.CONNECT_TIME);
        } catch (Exception e) {
            this.log.error(e.getMessage());
            return false;
        }
    }


}
