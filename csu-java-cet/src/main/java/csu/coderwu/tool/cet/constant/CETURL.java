package csu.coderwu.tool.cet.constant;

/**
 * @author coderWu
 * @date 17-12-20 下午5:50
 */
public class CETURL {

    public static final String IP = "202.197.61.241";

    public static final String UTL_ENCODE = "gb2312";

    public static final String CET_HISTORY = "http://202.197.61.241/engfen.asp";

}
