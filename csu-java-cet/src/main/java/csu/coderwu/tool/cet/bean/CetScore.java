package csu.coderwu.tool.cet.bean;

import csu.coderwu.tool.common.bean.BaseStudent;

/**
 * @author coderWu
 * @date 17-12-20 下午7:21
 */
public class CetScore {

    private BaseStudent student;
    private String date;

    /**
     * 准考证号
     */
    private String examNumber;

    /**
     * 证书编号
     */
    private String certificateId;

    private int score;

    private String level;

    public CetScore() {
    }

    public CetScore(BaseStudent student) {
        this.student = new BaseStudent(student.getXh(), student.getName());
    }

    public BaseStudent getStudent() {
        return student;
    }

    public CetScore setStudent(BaseStudent student) {
        this.student = new BaseStudent(student.getXh(), student.getName());
        return this;
    }

    public String getDate() {
        return date;
    }

    public CetScore setDate(String date) {
        this.date = date;
        return this;
    }

    public String getExamNumber() {
        return examNumber;
    }

    public CetScore setExamNumber(String examNumber) {
        this.examNumber = examNumber;
        return this;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public CetScore setCertificateId(String certificateId) {
        this.certificateId = certificateId;
        return this;
    }

    public int getScore() {
        return score;
    }

    public CetScore setScore(int score) {
        this.score = score;
        return this;
    }

    public String getLevel() {
        return level;
    }

    public CetScore setLevel(String level) {
        this.level = level;
        return this;
    }
}
