# csu校园功能开发工具包
包括四六级成绩查询、教务信息查询

### csu-java-es
教务系统信息查询

*getCourseSchedule(String xh, String pwd, String semester, int week)*
semester 学期
格式为:"x-y-z" 代表x-y学年第z学期
如 "2017-2018-1" 为 2017-2018第一学期
week 教学周次 int类型

*getStudyRoom(String campusId)*
campusId 为校区id其值为

| 校区  | campusId  |
| ------------ | ------------ |
| 校本部  |  1 |
| 南校区  |  2 |
| 铁道校区  |  3 |
| 湘雅新校区  |  4 |
| 湘雅老校区  |  5 |
| 湘雅医院  |  6 |
| 湘雅二医院  |  7 |
| 湘雅三医院  |  8 |
| 新校区  |  9 |
