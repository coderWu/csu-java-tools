package csu.coderwu.tool.common.bean;

/**
 * @author : coderWu
 * @date : Created on 11:12 2018/5/19
 */
public class BaseStudent {

    /**
     * 学号
     */
    protected String xh;
    protected String name;


    public BaseStudent() {
    }

    public BaseStudent(String xh, String name) {
        this.xh = xh;
        this.name = name;
    }

    public String getXh() {
        return xh;
    }

    public BaseStudent setXh(String xh) {
        this.xh = xh;
        return this;
    }

    public String getName() {
        return name;
    }

    public BaseStudent setName(String name) {
        this.name = name;
        return this;
    }
}
